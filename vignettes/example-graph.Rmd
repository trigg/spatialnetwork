---
title: "Create a spatial network over waters"
author: "Robert Trigg"
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Vignette Title}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

## Spatial Networks in R

This is a package to create spatial networks in marine, lacustrine, and fluvial environments. This package uses R's existing graph and spatial tools to automagically create a spatial network who's edges and vertices are only in areas of water, and who's edges have weight attributes equivalent to their lengths on the WGS84 ellipsoid.

## Getting Started

You must have a map or shapefile of the area in which you wish to create a spatial network, and have it loaded into R as a SpatialPolygonsDataFrame.

There are [many](https://gis.stackexchange.com/questions/19064/how-to-open-a-shapefile-in-r) [tutorials](https://www.r-bloggers.com/r-and-gis-working-with-shapefiles/) and [guides](https://www.nceas.ucsb.edu/scicomp/usecases/ReadWriteESRIShapeFiles) out there to help you learn to load such data into R.

## Prerequisites

This package depends on geosphere (>= 1.5.5), igraph (>= 1.0.1), maptools (>= 0.9.2), rgeos (>= 0.3.23), and sp (>= 1.2.4).

If you do not yet have rgeos installed, first you must install [GEOS](http://trac.osgeo.org/geos/).

To install rgdal you must first install [GDAL](http://trac.osgeo.org/gdal/wiki/DownloadingGdalBinaries) and [PROJ4](http://proj4.org/).

This package has been tested with R versions >= 3.4.1 (https://www.r-project.org/). The package will probably work with older versions of R, but I can't make any promises.


## Creating and plotting a spatial network

Here's an example using a map from [Natural Earth](http://www.naturalearthdata.com/downloads/50m-physical-vectors), using the 50m land polygons. We must load the shapefile, here I am loading an example shalefile that is included in this package. Then, decide on your grid size (in degrees), and the spatial extent you want the spatial network to have.

```{r, results = 'hide', message = FALSE}
library(spatialnetwork)

my_spdf <- rgdal::readOGR("../inst/extdata/ne_50m_land/",layer = "ne_50m_land")

grid_size <- 0.5 

# The Mediterranean Sea
lat <- seq(29, 46, grid_size)
lon <- seq(-5.5, 37, grid_size)

```

The creation of this spatial network takes less than 10 seconds on my machine; of course with higher-resolution shapefiles or a smaller grid size, it could take much longer The number of vertices and edges in the graph increases exponentially as the grid size decreases. It is recommended that you test how long it takes on your machine to create a spatial network with a larger grid size before creating a spatial network with more than 10k vertices [^1]. 

```{r}
test_graph <- setup_graph(lon, lat, grid_size, my_spdf, remove_edges = TRUE)

```


Now we plot the spdf and the spatial network. Note the use of spatialnetwork's `extract_coords_g` function to define the layout of the spatial network.

```{r fig1, fig.height = 3, fig.width = 7}
par(mgp=c(2.2,0.45,0), tcl=-0.4, mar=c(0,0,0,0))
sp::plot(my_spdf, xlim = range(lon), ylim = range(lat), col = "grey")
plot(test_graph, layout = extract_coords_g(test_graph), vertex.size = 1,
     vertex.label = NA, rescale = FALSE, add = TRUE)
```

[^1]: For example, on my machine it took over an hour to create a spatial network with >100k vertices.

