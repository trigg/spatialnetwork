# Spatial Networks in R

This is a package to create spatial networks in marine, lacustrine, and fluvial environments. This package uses R's existing graph and spatial tools to automagically create a spatial network who's edges and vertices are only in areas of water, and who's edges have weight attributes equivalent to their lengths on the WGS84 ellipsoid. 


## Getting Started

You must have a map or shapefile of the area in which you wish to create a spatial network, and have it loaded into R as a SpatialPolygonsDataFrame. 

There are [many](https://gis.stackexchange.com/questions/19064/how-to-open-a-shapefile-in-r) [tutorials](https://www.r-bloggers.com/r-and-gis-working-with-shapefiles/) and [guides](https://www.nceas.ucsb.edu/scicomp/usecases/ReadWriteESRIShapeFiles) out there to help you learn to load such data into R. 


### Prerequisites
This package depends on geosphere (>= 1.5.5), igraph (>= 1.0.1), maptools (>= 0.9.2), rgeos (>= 0.3.23), and sp (>= 1.2.4). 

If you do not yet have rgeos installed, first you must install [GEOS](http://trac.osgeo.org/geos/). 

While this package does not directly use rgdal, I do use it in the example below. To install rgdal you must first install [GDAL](http://trac.osgeo.org/gdal/wiki/DownloadingGdalBinaries) and [PROJ4](http://proj4.org). 

This package has been tested with R versions >= 3.4.1 (https://www.r-project.org/). The package will probably work with older versions of R, but I can't make any promises.


### Installing
To install the package from gitlab, you must have the [devtools](https://CRAN.R-project.org/package=devtools) package installed. Then simply run:
 
```
devtools::install_git("https://gitlab.com/trigg/spatialnetwork", ref = v0.1.0)
```


Here's an example using a map from [Natural Earth](http://www.naturalearthdata.com/downloads/50m-physical-vectors), using the 50m land polygons. Unzip the file and...


```
library(spatialnetwork)
library(rgdal) # my prefered package for loading shapefiles

# Load Shapefile - change this line to point to your data and/or 
# if you'd rather use the maptools or PBSmapping packages 
my_spdf = rgdal::readOGR(dsn="the/path/to/your/shapefiles/",
                         layer="ne_50m_land")


grid_size = 0.5 # size (in degrees) of grid

# The Mediterranean Sea
lat <- seq(29, 46, grid_size)
lon <- seq(-5.5, 37, grid_size)

# This takes less than 10 seconds on my machine, of course with 
# higher-resolution shapefiles or a smaller grid size, it could take 
# much longer. 
test_graph <- setup_graph(lon, lat, grid_size, my_spdf,
                          remove_edges = TRUE)

# Let's plot this thing!
# Note the use of spatialnetwork's "extract_coords_g" function in the
# layout of the spatial network.
sp::plot(my_spdf, xlim = range(lon), ylim = range(lat), col = "grey")
plot(test_graph, layout = extract_coords_g(test_graph), vertex.size = 10,
     vertex.label = NA, rescale = FALSE, add = TRUE)

```

![](/uploads/405fd7020b00971ab55383342c1e4a5b/graph_plot.png)

## Built With

* [roxygen2](https://CRAN.R-project.org/package=roxygen2) - [Doxygen](http://www.stack.nl/~dimitri/doxygen/)-style documentation generation for R.
* [devtools](https://www.rstudio.com/products/rpackages/devtools/) - A handy R package that makes package development easier.
* [igraph](http://igraph.org/) - *The* library to use for creating and working with networks.
* [rgeos](https://CRAN.R-project.org/package=rgeos) - The R interface to... 
* [geos](http://trac.osgeo.org/geos/) - Geometry Engine, Open Source

## Contributing

Please read the [code of conduct](CODE_OF_CONDUCT.md) and the details on [contributing](CONTRIBUTING.md).

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/trigg/spatialnetwork/tags). 

## Authors

* **Robert Trigg** - *Initial work* - [rtrigg](https://gitlab.com/trigg)

Here's the list of [contributors](https://gitlab.com/trigg/spatialnetwork/graphs/master) who participated in this project.

## License

This project is licensed under the GNU GPLv3 - see the [LICENSE](LICENSE) file for details.

## Acknowledgments

* [Robert Leaf](https://leaffisherylab.com/about/robert-t-leaf/), my major professor. 
* Francois Bastardie and others' (2010) work on the "Effects of fishing effort allocation scenarios on energy efficiency and profitability: an individual-based model applied to Danish fisheries" ([pdf](https://www.researchgate.net/profile/Francois_Bastardie/publication/222414187_Effects_of_Fishing_Effort_Allocation_Scenarios_on_Energy_Efficiency_and_Profitability_An_Individual-Based_Model_Applied_to_Danish_Fisheries/links/02bfe5132052197dbc000000.pdf)), which inspired me to write my own package to create a spatial network.

