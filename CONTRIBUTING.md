### Introduction
Thanks for your interest in contributing to the spatialnetworks package. Please read the following sections to learn how to work on the project. All contributors are expected to follow the [code of conduct](CODE_OF_CONDUCT.md), so please keep things civil :)

Following these guidelines ensures that developers respect each other for the time and effort they put in developing this package, responding to issues, and making important changes.

This package, `spatialnetworks`, is an open source project and contributions from users and other developers are welcome. Not all contributions have to involve coding; they can take the form of working on the documentation, blog posts on how you used the package, submitting bugs, and feature requests. 


### Ground Rules
* Ensure cross-platform compatibility for every change that's accepted. Windows, Mac, and Linux.
* Create issues for any major changes and enhancements that you wish to make. Discuss things transparently and get community feedback.
* Keep feature versions as small as possible, preferably one new feature per version.
* Do not add additional dependencies on other packages.
* Be welcoming to newcomers and encourage diverse new contributors from all backgrounds. See the [Code of Conduct](CODE_OF_CONDUCT.md).
* Do not commit changes to files that are irrelevant to your feature or bugfix (eg: `.gitignore`).
* Be aware that the pull request review process is not immediate.


### Your First Contribution - What's a pull request?
Are you new to git, gitlab, and/or contributing to open source projects? That's okay, everyone was new at this at some point! [Here](https://git-scm.com/book/en/v2) are some [resources](https://about.gitlab.com/2016/06/16/fearless-contribution-a-guide-for-first-timers/) that should help you out. 

### How to report a bug
You need to [create a GitLab account](https://gitlab.com/) to be able to [create new issues](https://docs.gitlab.com/ee/user/project/issues/create_new_issue.html) and participate in the discussion.

Please answer these six questions when reporting a bug:

1. What version of R are you using?
2. What version of `spatialnetwork` are you using?
3. What operating system are you using?
4. What did you do?
5. What did you expect to see?
6. What did you see instead?


### Code review process
Currently, [Robert Trigg](https://github.com/rtrigg) reviews and approves all changes. Expect to be contacted within a week, if not sooner. After feedback has been given, responses are expected within two weeks. After two weeks I reserve the right to close the pull request if it isn't showing any activity. 

### Sauce 
Like this contribution page? [Here's](https://github.com/nayafia/contributing-template/blob/master/CONTRIBUTING-template.md) the template to write your own. 
